package ru.t1.gorodtsova.tm.api.service.dto;

import ru.t1.gorodtsova.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.gorodtsova.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> extends IDtoService<M>, IUserOwnedDtoRepository<M> {
}
