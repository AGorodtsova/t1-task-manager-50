package ru.t1.gorodtsova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId, @NotNull Comparator<TaskDTO> comparator);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
