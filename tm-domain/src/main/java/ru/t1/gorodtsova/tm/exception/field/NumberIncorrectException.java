package ru.t1.gorodtsova.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value, @NotNull final Throwable cause) {
        super("Error! This value '" + value + "' is incorrect...");
    }

}
